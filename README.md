## 前言

由于工作生活节奏不同，如今很多关系稍疏远的亲戚之间来往并不多。因此放假回家过年时，往往会搞不清楚哪位亲戚应该喊什么称呼，很是尴尬。然而搞不清亲戚关系和亲戚称谓的不仅是小孩，就连年轻一代的大人也都常常模糊混乱。

“中国家庭称谓计算器”为你避免了这种尴尬，只需简单的输入即可算出称谓。输入框兼容了不同的叫法，你可以称呼父亲为：“老爸”、“爹地”、“老爷子”等等，方便不同地域的习惯叫法。快捷输入按键，只需简单的点击即可完成关系输入，算法还支持逆向查找称呼哦～！

## 三方库的使用

请查看[index](https://gitee.com/nutpi/relationship/blob/master/entry/src/main/ets/pages/Index.ets)文件

## 三方库使用说明

见[README](https://gitee.com/nutpi/relationship/blob/master/library/README.md)

## 致谢

致谢坚果派的各位小伙伴的协助，才有了这个三方库。
本库由 [刘张豪](https://gitee.com/an-ran0),完成迁移。



